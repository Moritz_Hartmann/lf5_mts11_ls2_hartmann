import math

def isPrime(n)->bool:
    """
    checks if a given number is prime

    :param n: number to check
    :return: boolean wheter the number is prime or not
    """
    if n<=1:
        return False

    sqrtPrime = int(math.sqrt(n))
    for i in range(2,sqrtPrime+1):
        if n%i == 0:
            return False
    return True