from unittest import TestCase

import PrimGenerator


class TestPrimGenerator(TestCase):
    def test_generate_prim_numbers(self):
        """Checks the given numbers of numbersToTest and compares the return value of booleanIsPrimList in range of 1-10.000."""
        numbersToTest = [1, 2, 5011, 9973, 10000]
        referenceValues = [False, True, True, True, False]
        _primGenerator = PrimGenerator.PrimGenerator()
        _primGenerator.generatePrimNumbers(100)
        counter = 0
        for number in numbersToTest:
            print(_primGenerator.booleanIsPrimList)
            self.assertEquals(_primGenerator.booleanIsPrimList[number-1],referenceValues[counter])
            counter+=1
