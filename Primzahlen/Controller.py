from PyQt5.QtGui import QColor

import PrimGenerator


class Controller:
    def __init__(self,view):
        """Constructor. Initializes the connections between buttons and the gui. Initializes the primGenerator.
        :param view: the gui to connect the controller to"""
        self.view = view
        self.view.button.clicked.connect(self.drawSelectedPrimes)
        self.primes = PrimGenerator.PrimGenerator()



    def drawSelectedPrimes(self):
        """Draws the primes to the Grid.
            """
        #Clear the canvas
        self.view.resetCanvas()

        #Try to read input values from Resolution TextEdit
        try:
            resolutionY = int(self.view.ResolutionYText.text())
        except:
            self.view.ResolutionYText.setText("Not an Integer")
            return
        try:
            resolutionX = int(self.view.ResolutionXText.text())
        except:
            self.view.ResolutionXText.setText("Not an Integer")
            return

        #Set the Resolution

        self.view.setResolution(resolutionX,resolutionY)

        try:
            startvalue = int(self.view.text.text())
            _range = resolutionY * resolutionX

            startvalue = self.primes.calcStartvalue(startvalue, _range)
            print(startvalue,_range)

            numberList = self.primes.generatePrimNumbers(startvalue,_range)
            self.view.label.setOffset(startvalue)

        except:
            self.view.text.setText("No valid Number")
            return

        self.view.numberRange.setText(f"Range:{startvalue}-{startvalue+_range-1}")

        _numberValue = 0

        for number in numberList:
            #if number is prime -> draw it
            #self.view.setBorders(number,QColor(200, 200, 200, 255))
            if number:
                self.view.drawToGui(_numberValue,QColor(0,255,0,255))
            else:
                #draws a black rectangle in the place instead.
                self.view.drawToGui(_numberValue, QColor(0, 0, 0, 255))
            _numberValue+=1

        #update canvas
        self.view.label.update()


