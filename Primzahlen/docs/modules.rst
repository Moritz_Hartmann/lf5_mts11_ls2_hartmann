Primzahlen
==========

.. toctree::
   :maxdepth: 4

   Controller
   MainGui
   MyMath
   PrimGenerator
   main
   test_MyMath
   test_PrimGenerator
