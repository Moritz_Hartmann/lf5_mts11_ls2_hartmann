import math

from PyQt5.QtCore import QRect
from PyQt5.QtGui import QPixmap, QColor, QPainter, QCursor
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QDialog, QToolButton, QFrame, QLineEdit


class mainGui(QWidget):
    def __init__(self):
        """Main Gui of size 800x800. Im using a Canvas instead of a gridLayout due to the fact that a grid with more than 6800 widgets is going to crash the application."""
        #init parentclass Qwidget
        super().__init__()

        #init Resolution
        self.ResolutionX = 100
        self.ResolutionY = 100
        self.sizeOfRectX = int(800 / self.ResolutionX)
        self.sizeOfRectY = int(800 / self.ResolutionY)

        self.numberRange = QLineEdit(self)
        self.numberRange.setGeometry(QRect(850, 600, 149, 30))
        self.numberRange.setReadOnly(True)
        self.numberRange.setText("No Range selected")
        self.numberRange.setStyleSheet("QLineEdit {border: None; background-color: rgba(0, 0, 0, 0);}")

        self.resXLabel=QLabel(self)
        self.resYLabel = QLabel(self)
        self.primeLabel = QLabel(self)
        self.resYLabel.setText("Y-Res:")
        self.resXLabel.setText("X-Res:")
        self.primeLabel.setText("Number:")

        self.resXLabel.setGeometry(QRect(810, 200,50,30))
        self.resYLabel.setGeometry(QRect(810, 250,50,30))
        self.primeLabel.setGeometry(QRect(810, 400,50,30))
        
        #size of application
        self.setFixedSize(1000,800)

        #Button for starting the task
        self.button = QToolButton(self)
        self.button.setGeometry(QRect(850,500,101,30))
        self.button.setText("Run")
        self.button.setToolTip("Starts the rendering")

        #Number input
        self.text = QLineEdit(self)
        self.text.setText("Enter Number")
        self.text.setGeometry(QRect(850,400,101,30))
        self.button.setToolTip("Enter a number. Number will be floored to next multiply of 10k.")

        #Resolution input
        self.ResolutionXText = QLineEdit(self)
        self.ResolutionXText.setText("100")
        self.ResolutionXText.setGeometry(QRect(850, 200, 101, 30))
        self.ResolutionYText = QLineEdit(self)
        self.ResolutionYText.setText("100")
        self.ResolutionYText.setGeometry(QRect(850,250,101,30))

        #Create a Canvas in a label widget and fill it with black background color
        self.label = Canvas(self, resolutionX=self.ResolutionX, sizePerRectX=self.sizeOfRectX, sizePerRectY=self.sizeOfRectY)
        self.canvas = QPixmap(800, 800)
        color = QColor(200, 200, 200, 255)
        self.canvas.fill(color)
        self.label.setPixmap(self.canvas)

    def setResolution(self,resolutionX,resolutionY):
        """Sets the resolution of the Matrix.

        :param resolutionX: resolution in x direction
        :param resolutionY: resolution in y direction
        """
        try:
            self.ResolutionX = resolutionX
            self.ResolutionY = resolutionY
            self.sizeOfRectX = int(800 / resolutionX)
            self.sizeOfRectY = int(800 / resolutionY)


            self.label.setSizePerRectY(self.sizeOfRectY)
            self.label.setSizePerRectX(self.sizeOfRectX)
            self.label.setResolutionX(self.ResolutionX)

        except:
            return



    def resetCanvas(self,color=QColor(200, 200, 200, 255)):
        """Resets the canvas. This color will be the grid border color"""
        self.canvas.fill(color)
        self.label.setPixmap(self.canvas)



    def drawToGui(self,number,color=QColor(0,0,0,255)):
        """faerbt 8x8 Pixel auf der Canvas ein


        :param number: number that should be colored on the Canvas
        :param color: color of the highlighted numbers
        """
        x,y = self.numberToXYCoordinates(number)

        painter = QPainter(self.label.pixmap())


        #set the rectangle color
        painter.fillRect(x * self.sizeOfRectX+1, y * self.sizeOfRectY+1, self.sizeOfRectX-2, self.sizeOfRectY-2, color)
        painter.end()

    def numberToXYCoordinates(self,number)->tuple[int,int]:
        """Converts the number to a coordinate on the canvas

        :param number: calculates the coordinates that reference this number
        :returns: x, y coordinate"""
        x = (number) % self.ResolutionX
        y = int((number) / self.ResolutionX)

        return x, y

class Canvas(QLabel):

    def __init__(self,parent=None,_offset=1,resolutionX=100,sizePerRectX=8,sizePerRectY=8):
        """Creates a Label with a custom Tooltip

        :param _offset: is the offset for the tooltip number
        :param resolutionX: Resolution in X Direction
        :param sizePerRectX: dimension of one rectangle in x direction
        :param sizePerRectY: dimension of one rectangle in y direction
        """
        super().__init__(parent)
        self.cursor = QCursor()
        self.setMouseTracking(True)
        self._offset = _offset
        self.resolutionX = resolutionX
        self.sizePerRectX = sizePerRectX
        self.sizePerRectY = sizePerRectY

    def setSizePerRectX(self,x):
        self.sizePerRectX = x

    def setSizePerRectY(self,y):
        self.sizePerRectY = y

    def setResolutionX(self,x):
        self.resolutionX = x

    def setOffset(self,_offset):
        self._offset = _offset

    def cursorPositionToNumber(self,x ,y)->int:
        """Translates the current cursor position to a number.

        :param x: x coordinate
        :param y: y coordinate
        :returns: int that belongs to these coordinates

        """

        number = math.ceil(x / self.sizePerRectX) + int(y / self.sizePerRectY) * self.resolutionX+self._offset-1

        return number

    def mouseMoveEvent(self, event):
        """Creates the tooltips. Calculates them from position. Overwrites the mouseMoveEvent from its parent"""
        point = self.mapFromGlobal(self.cursor.pos())
        y = point.y()
        x = point.x()
        number = self.cursorPositionToNumber(x,y)
        self.setToolTip(f"{number}")
