import sys
from PyQt5.QtWidgets import QApplication
import PyQt5
import MainGui
import Controller
import PrimGenerator

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainGui.mainGui()
    window.show()
    Controller = Controller.Controller(window)
    app.exec()