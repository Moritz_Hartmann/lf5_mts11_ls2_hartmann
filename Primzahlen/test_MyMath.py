from unittest import TestCase

import MyMath


class Test(TestCase):
    def test_is_prime(self):
        """Checking which of the values of the list numbersToTest are Primes. Compares them to the referenceValue. Translates one by one."""
        numbersToTest = [1,2,5011,9973,10000]
        referenceValues = [False,True,True,True,False]
        for iterator in range(0,len(numbersToTest)):
            self.assertEquals(referenceValues[iterator],MyMath.isPrime(numbersToTest[iterator]))

