import MyMath


class PrimGenerator:
    def __init__(self):
        """Constructor"""
        self.booleanIsPrimList = []

    def calcStartvalue(self,startvalue,_range):
        """Calculates the startvalue for the primenumbers

        :param startvalue: the input of the user
        :param _range: the given range
        :returns:  the startvalue floored to the multiple of range closest to int
        """
        startvalue = 1 + int(startvalue / (_range+1)) * _range
        return startvalue

    def generatePrimNumbers(self,startvalue=1,_range=10000)->list[bool]:
        """
        rounding the startvalue to the next lowest multiple of 10k and checks the next 10k numbers for primes. Returns a list of 10k
        numbers and marks primes with the value True.

        :param startvalue: Starting value of the array
        :param _range: The range of Primnumbers that sould be calculated
        :returns: A boolean list of all numbers. Primes are marked with True
        """
        self.booleanIsPrimList = []
        for number in range(startvalue, startvalue+_range):
            self.booleanIsPrimList.append(MyMath.isPrime(number))

        return self.booleanIsPrimList