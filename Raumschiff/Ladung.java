import java.util.ArrayList;

public class Ladung {
    private String typ;
    private int anzahl;

    Ladung(String name, int anzahl){
        this.typ = name;
        this.anzahl = anzahl;
    }

    public String getName(){
        return typ;
    }

    public void setName(String name){
        this.typ = name;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl){
        this.anzahl = anzahl;
    }

    /**
     * liefert die eindeutige Kennung der Ladung zurueck
     * @return
     */
    @Override
    public String toString(){
        String string = this.getClass().getName()+" Typ: "+typ+" Anzahl: "+anzahl;
        return string;
    }
}
