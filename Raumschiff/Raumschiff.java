import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
    private String name;
    private double energieversorgung;
    private double schilde;
    private double lebenserhaltungssysteme;
    private double huelle;
    private int photonentorpedo;
    private int reparturAndroiden;
    private ArrayList<String> logbuch;
    private ArrayList<Ladung> ladungsverzeichnis;
    private static ArrayList<String> broadcastKommunikator = new ArrayList<>();

    /**
     * Erstellt das Raumschiff. Schilde, Lebenserhaltung, Huelle, Energieversorgung mit Werten von 0-1 (Prozent/100). Photonen sind direkt geladen und sind feuerbereit.
     * @param name
     * @param energieversorgung
     * @param schilde
     * @param lebenserhaltungssysteme
     * @param huelle
     * @param photonentorpedo
     * @param reparturAndroiden
     * @param ladungsverzeichnis
     * @param logbuch
     */
    Raumschiff(String name, double energieversorgung,double schilde, double lebenserhaltungssysteme, double huelle, int photonentorpedo, int reparturAndroiden, ArrayList<Ladung> ladungsverzeichnis, ArrayList<String>logbuch){
        this.name = name;
        this.energieversorgung = energieversorgung;
        this.schilde = schilde;
        this.lebenserhaltungssysteme = lebenserhaltungssysteme;
        this.huelle = huelle;
        this.photonentorpedo = photonentorpedo;
        this.reparturAndroiden = reparturAndroiden;
        this.ladungsverzeichnis = ladungsverzeichnis;
        this.logbuch = logbuch;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Nimmt die Prozente als double von 0 - 1. Min Wert = 0.0 | Max Wert = 1.0
     * @param energieversorgung
     */
    public void setEnergieversorgung(double energieversorgung) {
        if (energieversorgung<=0){
            this.energieversorgung=0.0;
        }else if(energieversorgung>=1.0){
            this.energieversorgung=1.0;
        }
        else {
            this.energieversorgung = energieversorgung;
        }
    }


    public double getEnergieversorgung() {
        return energieversorgung;
    }

    /**
     * Nimmt die Prozente als double von 0 - 1. Min Wert = 0.0 | Max Wert = 1.0
     * @param schilde
     */
    public void setSchilde(double schilde) {
        if(schilde<=0){
            this.schilde=0.0;
        }else if(schilde>=1.0){
            this.schilde=1.0;
        }
        else{
            this.schilde = schilde;
        }
    }


    public double getSchilde() {
        return schilde;
    }

    /**
     * Nimmt die Prozente als double von 0 - 1. Min Wert = 0.0 | Max Wert = 1.0
     * @param lebenserhaltungssysteme
     */
    public void setLebenserhaltungssysteme(double lebenserhaltungssysteme) {
        if(lebenserhaltungssysteme<=0){
            this.lebenserhaltungssysteme=0.0;
        }else if(lebenserhaltungssysteme>=1.0){
            this.lebenserhaltungssysteme=1.0;
        }
        else {
            this.lebenserhaltungssysteme = lebenserhaltungssysteme;
        }
    }

    public double getLebenserhaltungssysteme() {
        return lebenserhaltungssysteme;
    }

    /**
     * Nimmt die Prozente als double von 0 - 1. Min Wert = 0.0 | Max Wert = 1.0
     * @param huelle
     */
    public void setHuelle(double huelle) {
        if(huelle<=0){
            this.huelle=0.0;
        }else if(huelle>=1.0){
            this.huelle=1.0;
        }
        else {
            this.huelle = huelle;
        }
    }


    public double getHuelle() {
        return huelle;
    }


    public void setPhotonentorpedo(int photonentorpedo) {
        this.photonentorpedo = photonentorpedo;
    }


    public int getPhotonentorpedo() {
        return photonentorpedo;
    }


    public void setReparturAndroiden(int reparturAndroiden) {
        this.reparturAndroiden = reparturAndroiden;
    }


    public int getReparturAndroiden() {
        return reparturAndroiden;
    }

    /**
     * @param logbuch ArrayList<String>
     */
    public void setLogbuch(ArrayList<String> logbuch) {
        this.logbuch = logbuch;
    }


    public ArrayList<String> getLogbuch() {
        return logbuch;
    }

    /**
     * @param ladungsverzeichnis ArrayList<Ladungsverzeichnis>
     */
    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }


    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    /**
     * returned static braodcastKommunikator
     * @return broadcastKommunikator
     */
    public static ArrayList<String> getBroadcastKommunikator() {
        return broadcastKommunikator;
    }

    /**
     * Feuert einen Photonentorpedo ab, falls photonentorpedos vorhanden
     * @param raumschiff
     */
    public void photonentorpedoAbschiessen(Raumschiff raumschiff){
        if(this.photonentorpedo>0){
            nachrichtSenden("Photonentorpedo abgeschossen");
            this.photonentorpedo-=1;
        }else{
            nachrichtSenden("-=*Click*=-");
        }

        raumschiff.getroffenWerden();
    }


    /**
     * Feuert einen Schuss der Phaserkanone ab, falls Energie vorhanden.
     * @param raumschiff
     */
    public void phaserkanoneAbschiessen(Raumschiff raumschiff){
        if(this.energieversorgung>=0.5){
            this.setEnergieversorgung(this.getEnergieversorgung()-0.5);
            nachrichtSenden("Phaserkanone abgeschossen");
        }else{
            nachrichtSenden("-=*Click*=-");
        }

        raumschiff.getroffenWerden();
    }

    /**
     * Berechnet den Schaden an dem Schiff. Zuerst werden Schilde reduziert, sobald diese auf 0% dann werden huelle und energieversorgung angegriffen.
     * Berichtet dem Broadcastkommunikator
     */
    private void getroffenWerden(){
        System.out.println(name+" wurde getroffen!" );
        if (schilde<=0) {
            this.setHuelle(this.getHuelle() -0.5);
            this.setEnergieversorgung(this.getEnergieversorgung()-0.5);
            if(this.huelle<=0){
                nachrichtSenden("Lebenserhaltungssysteme wurden zerstoert");
            }
        }else{
            this.setSchilde(this.getSchilde()-0.5);
        }
    }

    /**
     * Fuegt die Nachricht dem broadcastkommunikator hinzu.
     * @param nachricht
     */
    public void nachrichtSenden(String nachricht){
        //System.out.println(nachricht);
        broadcastKommunikator.add(nachricht);
    }

    /**
     * Fuegt die Ladung dem Ladungsverzeichnis des Schiffes hinzu.
     * @param ladung
     */
    public void ladungHinzufuegen(Ladung ladung){
        boolean vorhanden=false;
        for (Ladung _ladung:this.ladungsverzeichnis
             ) {
            if(_ladung.getName().equals(ladung.getName())){
                _ladung.setAnzahl(_ladung.getAnzahl()+ladung.getAnzahl());
                vorhanden = true;
            }
        }
        if(!vorhanden) {
            this.ladungsverzeichnis.add(ladung);
        }
    }

    /**
     *
     * @return logbuch
     */
    public ArrayList<String> logbuchEintraege(){return logbuch;}

    /**
     * Laedt photonentorpedos die noch nicht geladen wurden (mittels Konstruktor) aus dem Ladungsverzeichnis ins Schiff.
     * @param photonentorpedo
     */
    public void photonentorpedoLaden(int photonentorpedo){
        boolean check = false;
        int eingesetztePhotonentorpedos=0;
        for (Ladung _ladung:this.ladungsverzeichnis) {
            if(_ladung.getName()=="Photonentorpedo"){
                if(_ladung.getAnzahl()<=0){
                    break;
                } else if(photonentorpedo>=_ladung.getAnzahl()){
                    this.photonentorpedo = _ladung.getAnzahl();
                    eingesetztePhotonentorpedos=_ladung.getAnzahl();
                    _ladung.setAnzahl(0);
                    check=true;
                    break;
                }else if(photonentorpedo<_ladung.getAnzahl()){
                    this.photonentorpedo = photonentorpedo;
                    _ladung.setAnzahl(_ladung.getAnzahl()-photonentorpedo);
                    eingesetztePhotonentorpedos=photonentorpedo;
                    check=true;
                    break;
                }

            }
        }
        if(!check){
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtSenden("=*Click*=-");
        }else{
            System.out.println(eingesetztePhotonentorpedos+" Photonentorpedo(s) eingesetzt");
        }
    }

    /** Repariert die ausgewaehlten parameter und erhoeht diese um einen Zufaelligen Wert. Teilt die Auftraege unter den Androiden auf.
     * @param energieversorgung
     * @param huelle
     * @param schilde
     * @param anzahlAndroiden
     */
    public void reparaturauftrag(boolean energieversorgung, boolean huelle, boolean schilde, int anzahlAndroiden){
        int anzahlReparturaufraege=0;
        if(energieversorgung){
            anzahlReparturaufraege+=1;
        }
        if(huelle){
            anzahlReparturaufraege+=1;
        }
        if(schilde){
            anzahlReparturaufraege+=1;
        }
        Random rand = new Random();


        double zufallszahl = rand.nextInt(101)/100.0;
        if(this.reparturAndroiden<anzahlAndroiden){
            anzahlAndroiden=this.reparturAndroiden;
        }
        if(energieversorgung){
            this.setEnergieversorgung(this.getEnergieversorgung()+ zufallszahl*anzahlAndroiden/(1.0*anzahlReparturaufraege));
        }
        if(huelle){

            this.setHuelle( this.getHuelle()+zufallszahl*anzahlAndroiden/(1.0*anzahlReparturaufraege));
        }
        if(schilde){
            this.setSchilde(this.getSchilde()+ zufallszahl*anzahlAndroiden/(anzahlReparturaufraege*1.0));
        }
    }

    /**
     * Printet auf der Konsole alle Attribute des Schiffes aus (Name, Energiversorgung, Schilde, Lebenserhaltung, Huelle, Photonentorpedo)
     */
    public void zustandsausgabe(){
        //System.out.println("Name "+name+ "\n\tEnergieversorgung: "+energieversorgung+ "\n\tSchilde: "+schilde+ "\n\tLebenserhaltung: "+lebenserhaltungssysteme+ "\n\tHuelle: "+huelle+"\n\tPhotonentorpedo: "+ photonentorpedo+"\n");
        System.out.printf("Name %s\n\tEnergieversorgung: %.2f\n\tSchilde: %.2f\n\tLebenserhaltung: %.2f\n\tHuelle: %.2f\n\tPhotonentorpedo: %d\n",name,energieversorgung,schilde,lebenserhaltungssysteme,huelle,photonentorpedo);

    }

    /**
     * Printet auf der Konsole jede Ladung inerhalb des Schiffs
     */
    public void ausgabeLadungsverzeichnis(){
        for (Ladung ladung: this.ladungsverzeichnis) {
            System.out.println(ladung.toString());
        }
        System.out.println();
    }


    /**
     * Entfernt Ladungen mit der Anzahl 0
     */
    public void ladungsverzeichnisAufrauemen(){
        for (Ladung ladung: this.ladungsverzeichnis) {
            if(ladung.getAnzahl()<=0){
                this.ladungsverzeichnis.remove(ladung);
            }
        }
    }

}
