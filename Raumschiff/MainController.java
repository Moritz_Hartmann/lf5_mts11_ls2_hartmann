import java.util.ArrayList;
import java.util.Random;

public class MainController {
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta",1.00,0.5,1.00,1.00,0,2,new <Ladung>ArrayList(),new <String>ArrayList());
        Raumschiff romulaner= new Raumschiff("IRW Khazara",0.5,0.5,1.0,1.0,2,2,new <Ladung>ArrayList(),new <String>ArrayList());
        Random random = new Random();
        double rand_1 = random.nextInt(101)/100.0;
        double rand_2 = random.nextInt(101)/100.0;
        double rand_3 = random.nextInt(101)/100.0;

        Raumschiff vulkanier= new Raumschiff("Ni' Var",rand_1,rand_2,1.0,rand_3,3,5,new <Ladung>ArrayList(),new <String>ArrayList());


        Ladung ferengiSchneckensaft = new Ladung("Ferengi Schneckensaft",200);
        klingonen.ladungHinzufuegen(ferengiSchneckensaft);
        Ladung bathlethKlingonenSchwert = new Ladung("Bath'leth Klingonen Schwert",200);
        klingonen.ladungHinzufuegen(bathlethKlingonenSchwert);

        Ladung borgSchrott=new Ladung("Borg-Schrott",5);
        romulaner.ladungHinzufuegen(borgSchrott);
        Ladung roteMaterie=new Ladung("rote Materie",2);
        romulaner.ladungHinzufuegen(roteMaterie);
        Ladung plasmaWaffen=new Ladung("Plasma-Waffen",50);
        romulaner.ladungHinzufuegen(plasmaWaffen);

        Ladung Forschungssonde = new Ladung("Forschungssonde",35);
        vulkanier.ladungHinzufuegen(Forschungssonde);

        klingonen.photonentorpedoAbschiessen(romulaner);
        romulaner.phaserkanoneAbschiessen(klingonen);
        vulkanier.nachrichtSenden("Gewalt ist nicht logisch");
        klingonen.zustandsausgabe();
        klingonen.ausgabeLadungsverzeichnis();
        vulkanier.reparaturauftrag(true,true,true,5);
        vulkanier.photonentorpedoLaden(5);//haben nur bereits geladene Photonentorpedos
        vulkanier.ladungsverzeichnisAufrauemen();
        klingonen.photonentorpedoAbschiessen(romulaner);
        klingonen.photonentorpedoAbschiessen(romulaner);
        klingonen.zustandsausgabe();
        klingonen.ausgabeLadungsverzeichnis();
        romulaner.zustandsausgabe();
        romulaner.ausgabeLadungsverzeichnis();
        vulkanier.zustandsausgabe();
        vulkanier.ausgabeLadungsverzeichnis();
        System.out.println(Raumschiff.getBroadcastKommunikator());

    }
}
